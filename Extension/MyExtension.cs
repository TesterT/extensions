﻿using System;
using System.Collections.Generic;

namespace Extension
{
    public static class MyExtension
    {
        public static int SummaDigit(this int n)
        {
           int summa = 0;
            if (n > 0)
            {
                while (n > 0)
                {
                    summa += n % 10;
                    n /= 10;
                }
            }
            else
            {
                n = Math.Abs(n);
                while (n > 0)
                {
                    summa += n % 10;
                    n /= 10;
                }
            }
            return summa;
        }
        
        public static ulong SummaWithReverse(this uint n)
        {
            uint summa = n;
            ulong reverse_number = 0;
            while (n > 0)
            {
                reverse_number = reverse_number * 10 + (n % 10);
                n /= 10;
            }
            return (summa + reverse_number);
        }
        
        
        public static int CountNotLetter(this string str)
        {
            int number = 0;
            foreach (char c in str)
                if (!((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')))  //check different combinations
                    number++;
            return number;
        }

        public static bool IsDayOff(this DayOfWeek day)
        {
            if (day == DayOfWeek.Sunday)
                return true;
            else if (day == DayOfWeek.Saturday)
                return true;
            else
                return false;         
        }

        public static IEnumerable<int> EvenPositiveElements(this IEnumerable<int> numbers)
        {

            foreach (int number in numbers)
            {
                if (number % 2 ==0 && number > 0)
                {
                    yield return number;
                }
            }
        }

    }
}
